const notifier = require('node-notifier');
const moment = require('moment');

const minute = 1000 * 60;
const interval_low = minute * .5;
const interval_high = minute * 1.25;

const startTime = moment();

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function message(title, message) {
  notifier.notify({
    'title': title,
    'message': message,
    'timeout': 5
  });
}

function tick() {
  const now = moment();
  const elapsedTime = startTime.from(now);
  const msg = `we started ${elapsedTime}`;
  message('Practice Time!', msg);
  console.info(msg);
}

function tickWithTimeout() {
  const delay = getRandomArbitrary(interval_low, interval_high);
  setTimeout(() => {
    tick();
    tickWithTimeout();
  }, delay);
}

tick();
tickWithTimeout();

